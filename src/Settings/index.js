// ===========================
//  Puerto
// ===========================
process.env.PORT = process.env.PORT || 3000;
// ===========================
//  Entorno
// ===========================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';
// ===========================
//  Subdominio
// ===========================
process.env.SUBDOMINIO =process.env.SUBDOMINIO||'http://localhost:3000';
