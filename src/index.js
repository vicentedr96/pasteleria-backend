import Variables from './Settings';
import Server from './Server';
import { Routes } from './Routes';

const server = Server.init(process.env.PORT);
server.app.use(Routes);
server.start(); 


