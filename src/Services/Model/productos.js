import { readFile } from 'fs/promises';

export default class Productos {
    static async obtenerProductos() {
        try {
            let data = JSON.parse(await readFile(`${__dirname}/AxoviaData.json`));
            return data;
        } catch (err) {
            throw new Error(err.message)
        }
    }
}