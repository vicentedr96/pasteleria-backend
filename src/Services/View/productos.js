import Servicios from "../Controller/productos";
import Helper from "../../Utils/Helpers/Validation";

let helper = new Helper();

export default class Productos {
    static async obtenerProductos(input) {
        let result = await Servicios.obtenerProductos(input)
            .then((resp) => {
                return helper.setStatusCorrect("", "Operacion Correcta", resp || [])
            })
            .catch(err => { return helper.setStatusIncorrect(err.message) })
        return result;
    }
}