import peticiones from "../Model/productos";
import generador from "../../Utils/Helpers/Generador";

export default class Productos {
    
    static async obtenerProductos(req) {
        try {
            let { name="", comboPrice = "TODOS", price = "TODOS", toppingType="", size="", ordenarPor = "ASC" } = req.body;
            let data = {
                name,
                comboPrice,
                price,
                toppingType: toppingType !== "" ? [toppingType] : ["0", "1", "2"],
                size: size !== "" ? [size] : ["M", "S", "L"],
                ordenarPor
            };
            let pt = await peticiones.obtenerProductos();
            pt = await this.filtroComun(pt, data);
            pt = await this.ordenarPor(pt, data);
            pt = await this.filtroPorNombre(pt, data);
            pt = await this.formateoDatos(pt);
            return pt;
        } catch (err) {
            throw new Error(err.message);
        }
    }

    static async formateoDatos(data) {

        if (data?.length == 0) {
            return data;
        } else {
            return data = data.map(function (item) {
                return {
                    ...item,
                    price: `${parseFloat(item.price).toFixed(2)}`,
                    comboPrice: `${parseFloat(item.comboPrice).toFixed(2)}`,
                    size: item.size === "L" ? "Grande" : item.size === "M" ? "Mediano" : "Pequeño",
                    name: generador.Capitalizar(item.name),
                    toppingType: item.toppingType === "1" ? "Betun Italiano" : item.toppingType === "2" ? "Chantilly" : "Fondeau"
                }
            });
        }
    }

    static async filtroComun(data, obj) {
        let { comboPrice, price, toppingType, size } = obj;
        let result = data.data.filter(
            item =>
                size.indexOf(item.size) > -1 &&
                toppingType.indexOf(item.toppingType) > -1 &&
                (price == "ECONÓMICOS" ? item.price < 600 : item.price > 0) &&
                (comboPrice == "ECONÓMICOS" ? item.comboPrice < 600 : item.comboPrice > 0)
        );
        return result;
    }

    static async ordenarPor(result, { ordenarPor }) {
        if (ordenarPor === "DESC") {
            result = result.sort(function (a, b) {
                return (b.price - a.price)
            })
        } 
        
        if (ordenarPor === "ASC") {
            result.sort(function (a, b) {
                return (a.price - b.price)
            })
        }
        
        return result;
    }

    static async filtroPorNombre(result, { name }) {
        return result.filter(item => item.name.indexOf(name) !== -1);
    }
}