import Server from "../Server";
import swaggerJsDoc from 'swagger-jsdoc';
import swaggerUI from 'swagger-ui-express';

const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Tienda de pasteles api',
      version: '1.0.0',
      description: "API DOCUMENTACION",
    },
  },
  servers: [{ url: `${process.env.SUBDOMINIO}` }],
  apis: ['./src/Routes/*.router.js'], 
};

const openapiSpecification = swaggerJsDoc(options);
const { app, server } = Server.getInstace();

app.use("/documentacion", swaggerUI.serve, swaggerUI.setup(openapiSpecification))
app.use('/productos', require('./productos.router'));

module.exports = { Routes: app, server };