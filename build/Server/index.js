"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _safe = _interopRequireDefault(require("colors/safe"));

var _path = _interopRequireDefault(require("path"));

var _cors = _interopRequireDefault(require("cors"));

var _http = _interopRequireDefault(require("http"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class Server {
  constructor(port) {
    this.port = port;
    this.app = (0, _express.default)();
    this.app.use((0, _cors.default)());
    this.apollo = null;
    this.convertData();
    this.server = _http.default.createServer(this.app);
  }

  static init(port) {
    return new Server(port);
  }

  publicFolder() {
    const publicPath = _path.default.resolve(__dirname, '../public');

    this.app.use(_express.default.static(publicPath));
  }

  convertData() {
    this.app.use(_express.default.urlencoded({
      extended: false
    }));
    this.app.use(_express.default.json({
      type: "*/*"
    }));
  }

  viewHbs() {
    this.app.set('view engine', 'hbs');
  }

  start() {
    this.server.listen(this.port, {
      log: false,
      origins: '*:*'
    }).on('error', e => {
      if (e.code !== 'EADDRINUSE' && e.code !== 'EACCES') {
        throw e;
      }

      console.error(_safe.default.bgGray('❌  Port ' + this.port + ' is busy'));
    }).on('listening', () => {
      console.log(_safe.default.bgBlue('✅ Listening on http://localhost:' + this.port));
    });
    this.publicFolder();
    this.viewHbs();
  }

  stop() {
    this.server.listen(this.port, function () {
      this.server.close();
    });
  }

  static getInstace() {
    return this.Server || (this.Server = new Server());
  }

}

exports.default = Server;