"use strict";

var _express = _interopRequireDefault(require("express"));

var _productos = _interopRequireDefault(require("../Services/View/productos"));

var _Validation = _interopRequireDefault(require("../Utils/Helpers/Validation"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = _express.default.Router();

let Validar = new _Validation.default();
/**
 * @swagger
 * tags:
 *    name: Pasteleria
 *    description: obtener pasteles por filtros
 */

/**
 *   @swagger
 *   components:
 *     schemas:
 *       PT:
 *         type: object
 *         properties:
 *          message:
 *            type: string
 *            example: Operación Correcta
 *          success:
 *            type: string
 *            example: true
 *          response:
 *            type: integer
 *            example: 200
 *          error:
 *            type: string
 *            example: ""
 *          data:
 *            type: object
 *            properties:
 *              name:
 *                type: string
 *                example: Pastel de zanahoria
 *              imageUrl:
 *                type: string
 *                example: https://axovia.mx/challenge/pastelzanahoria.jpeg
 *              description:
 *                type: string
 *                example: Lorem ipsum dolor sit amet 
 *              price:
 *                type: string
 *                example: $400.65  
 *              comboPrice:
 *                type: string
 *                example: $1300.56  
 *              toppingType:
 *                type: string
 *                example: Betun Italiano  
 *              size:
 *                type: string
 *                example: Grande  
*/

/**
 * @swagger
 * /pasteleria/obtener:
 *   post:
 *     description: Retorna la informacíon solicitada
 *     requestBody:
 *        required: false
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                name:
 *                  type: string
 *                  example: zarzamora
 *                comboPrice:
 *                  type: string
 *                  example: TODOS
 *                price:
 *                  type: string
 *                  example: TODOS
 *                toppingType:
 *                  type: string
 *                  example: 1
 *                size:
 *                  type: string
 *                  example: S
 *                ordenarPort:
 *                  type: string
 *                  example: DESC
 *     tags: [Pasteleria]
 *     responses:
 *       200:
 *         description: Operación Correcta
 *         content:
 *           application/json:
 *              schema:
 *                $ref: '#/components/schemas/PT'
 *       400:
 *         description: Operación Fallida   
 *         content:
 *           application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  success:
 *                    type: string
 *                    example: false
 *                  error:
 *                    type: string
 *                    example: ""  
 *                  response:
 *                    type: string
 *                    example: 400   
 * 
 * 
 */

router.post("/obtener", async (req, res) => {
  _productos.default.obtenerProductos(req).then(resp => {
    res.json(resp);
  }).catch(err => {
    res.status(400).json(Validar.setStatusIncorrect(err.message));
  });
});
module.exports = router;