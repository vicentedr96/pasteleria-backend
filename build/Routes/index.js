"use strict";

var _Server = _interopRequireDefault(require("../Server"));

var _swaggerJsdoc = _interopRequireDefault(require("swagger-jsdoc"));

var _swaggerUiExpress = _interopRequireDefault(require("swagger-ui-express"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const options = {
  definition: {
    openapi: '3.0.0',
    info: {
      title: 'Pasteleria api',
      version: '1.0.0',
      description: "API DOCUMENTACION"
    }
  },
  servers: [{
    url: `${process.env.SUBDOMINIO}`
  }],
  apis: ['./src/Routes/*.router.js']
};
const openapiSpecification = (0, _swaggerJsdoc.default)(options);

const {
  app,
  server
} = _Server.default.getInstace();

app.use("/api-docs", _swaggerUiExpress.default.serve, _swaggerUiExpress.default.setup(openapiSpecification));
app.use('/pasteleria', require('./productos.router'));
module.exports = {
  Routes: app,
  server
};