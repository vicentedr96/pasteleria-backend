"use strict";

var _Settings = _interopRequireDefault(require("./Settings"));

var _Server = _interopRequireDefault(require("./Server"));

var _Routes = require("./Routes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const server = _Server.default.init(process.env.PORT);

server.app.use(_Routes.Routes);
server.start();