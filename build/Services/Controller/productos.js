"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _productos = _interopRequireDefault(require("../Model/productos"));

var _Generador = _interopRequireDefault(require("../../Utils/Helpers/Generador"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class Productos {
  static async obtenerProductos(req) {
    try {
      let {
        name = "",
        comboPrice = "TODOS",
        price = "TODOS",
        toppingType = "",
        size = "",
        ordenarPor = "ASC"
      } = req.body;
      let data = {
        name,
        comboPrice,
        price,
        toppingType: toppingType !== "" ? [toppingType] : ["0", "1", "2"],
        size: size !== "" ? [size] : ["M", "S", "L"],
        ordenarPor
      };
      let pt = await _productos.default.obtenerProductos();
      pt = await this.filtroComun(pt, data);
      pt = await this.ordenarPor(pt, data);
      pt = await this.filtroPorNombre(pt, data);
      pt = await this.formateoDatos(pt);
      return pt;
    } catch (err) {
      throw new Error(err.message);
    }
  }

  static async formateoDatos(data) {
    var _data;

    if (((_data = data) === null || _data === void 0 ? void 0 : _data.length) == 0) {
      return data;
    } else {
      return data = data.map(function (item) {
        return { ...item,
          price: `${parseFloat(item.price).toFixed(2)}`,
          comboPrice: `${parseFloat(item.comboPrice).toFixed(2)}`,
          size: item.size === "L" ? "Grande" : item.size === "M" ? "Mediano" : "Pequeño",
          name: _Generador.default.Capitalizar(item.name),
          toppingType: item.toppingType === "1" ? "Betun Italiano" : item.toppingType === "2" ? "Chantilly" : "Fondeau"
        };
      });
    }
  }

  static async filtroComun(data, obj) {
    let {
      comboPrice,
      price,
      toppingType,
      size
    } = obj;
    let result = data.data.filter(item => size.indexOf(item.size) > -1 && toppingType.indexOf(item.toppingType) > -1 && (price == "ECONÓMICOS" ? item.price < 600 : item.price > 0) && (comboPrice == "ECONÓMICOS" ? item.comboPrice < 600 : item.comboPrice > 0));
    return result;
  }

  static async ordenarPor(result, {
    ordenarPor
  }) {
    if (ordenarPor === "DESC") {
      result = result.sort(function (a, b) {
        return b.price - a.price;
      });
    }

    if (ordenarPor === "ASC") {
      result.sort(function (a, b) {
        return a.price - b.price;
      });
    }

    return result;
  }

  static async filtroPorNombre(result, {
    name
  }) {
    return result.filter(item => item.name.indexOf(name) !== -1);
  }

}

exports.default = Productos;