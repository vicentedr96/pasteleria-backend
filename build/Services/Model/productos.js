"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _promises = require("fs/promises");

class Productos {
  static async obtenerProductos() {
    try {
      let data = JSON.parse(await (0, _promises.readFile)(`${__dirname}/AxoviaData.json`));
      return data;
    } catch (err) {
      throw new Error(err.message);
    }
  }

}

exports.default = Productos;