"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _productos = _interopRequireDefault(require("../Controller/productos"));

var _Validation = _interopRequireDefault(require("../../Utils/Helpers/Validation"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let helper = new _Validation.default();

class Productos {
  static async obtenerProductos(input) {
    let result = await _productos.default.obtenerProductos(input).then(resp => {
      return helper.setStatusCorrect("", "Operacion Correcta", resp || []);
    }).catch(err => {
      return helper.setStatusIncorrect(err.message);
    });
    return result;
  }

}

exports.default = Productos;