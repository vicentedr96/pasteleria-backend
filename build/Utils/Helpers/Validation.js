"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

class Validate {
  static Validate = null;
  x = null;

  constructor() {
    this.x = {
      message: "",
      success: "",
      error: "",
      response: "",
      token: "",
      data: ""
    };
  }

  setStatusIncorrect(err) {
    this.setError(err);
    this.setSuccess(false);
    this.setMessage("Operación fallida");
    this.setResponse(400);
    this.setToken("");
    return this.x;
  }

  setStatusCorrect(token = "", message = "Operación Correcta", data = "") {
    this.setError("");
    this.setSuccess(true);
    this.setMessage(message);
    this.setResponse(200);
    this.setToken(token);
    this.setData(data);
    return this.x;
  }

  setError(err) {
    this.x.error = err;
  }

  setSuccess(succ) {
    this.x.success = succ;
  }

  setMessage(msj) {
    this.x.message = msj;
  }

  setResponse(resp) {
    this.x.response = resp;
  }

  setToken(tok) {
    this.x.token = tok;
  }

  setData(dat) {
    this.x.data = dat;
  }

}

exports.default = Validate;